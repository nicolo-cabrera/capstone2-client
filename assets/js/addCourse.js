let formSubmit = document.querySelector("#createCourse");

formSubmit.addEventListener("submit", (e) => {
	e.preventDefault()

	let courseName = document.querySelector("#courseName").value;
	let description = document.querySelector("#courseDescription").value;
	let price = document.querySelector("#coursePrice").value;

	/*get the JWT from local storage*/
	let token = localStorage.getItem("token");

	
	fetch("https://frozen-badlands-97230.herokuapp.com/api/courses", {
		method : "POST",

		headers : {
			"Content-Type" : "application/json",
			"Authorization" : `Bearer ${token}`
		},

		body : JSON.stringify({
			name : courseName,
			description : description,
			price : price
		})

	})

	.then(res => res.json()) 
	.then(data => {
		return window.location.replace("./courses.html");
	})

})