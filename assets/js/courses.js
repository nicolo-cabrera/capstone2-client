let adminUser = localStorage.getItem("isAdmin");
let modalButton = document.querySelector("#adminButton");
let token = localStorage.getItem("token")
let cardFooter;

if (adminUser == "false" || !adminUser ) {
	modalButton.innerHTML = null;
	fetch("https://frozen-badlands-97230.herokuapp.com/api/courses")
	.then(res => res.json())
	.then(data => {
		if(data.length < 1) {
			courseData = "No courses available"
		} else {
			let CourseData;

			courseData = data.map(course => {
				cardFooter = 
							`
								<a href="./course.html?courseId=${course._id}" value="${course._id}" class="btn btn-primary text-white btn-block"> Go to Course </a>
							`
				return (

						`
							<div class="col-md-6 my-3">
								<div class="card">
									<div class="card-body">
										<h5 class="card-title">${course.name}</h5>
										<p class="card-text text-left">
											${course.description}
										</p>
										<p class="card-text text-right">
											${course.price}
										</p>
									</div>
									<div class="card-footer">
										${cardFooter}
									</div>
								</div>
							</div>
						`

					)

			}).join("")
			let container = document.querySelector("#coursesContainer");
			container.innerHTML = courseData;	
		}
	})

} else {
	let container = document.querySelector("#removeLink");
	removeLink.innerHTML = null;	
	modalButton.innerHTML = 
		`
			<div class = "col-md-2 offset-md-10"> 
				<a href="./addCourse.html" class="btn btn-block btn-primary">
					Add Course
				</a>
			</div>

		`

	fetch("https://frozen-badlands-97230.herokuapp.com/api/courses/admin", {
		method: "GET",
		headers : {
			"Authorization" : `Bearer ${token}`
		}
	})

	.then(res => res.json())
	.then(data => {
		if(data.length < 1) {
			courseData = "No courses available"
		} else {
			let CourseData;

			courseData = data.map(course => {
			
				if(course.isActive === true) {

					cardFooter = 
					`
						<a href="./course.html?courseId=${course._id}" value="${course._id}" class="btn btn-info text-white btn-block"> Go to Course </a>
						<a href="./editCourse.html?courseId=${course._id}" value=${course._id} class = "btn btn-success text-white btn-block"> Edit Course </a>
						<a href="./deleteCourse.html?courseId=${course._id}" value=${course._id} class = "btn btn-danger text-white btn-block"> Archive Course </a>
					`

				} else {

					cardFooter = 
					`	
						<a href="./course.html?courseId=${course._id}" value="${course._id}" class="btn btn-info text-white btn-block"> Go to Course </a>
						<a href="./editCourse.html?courseId=${course._id}" value=${course._id} class = "btn btn-success text-white btn-block"> Edit Course </a>
						<a href="./unarchive.html?courseId=${course._id}" value=${course._id} class = "btn btn-primary text-white btn-block"> Unarchive Course </a>
						

					`
				}

							
				return (

						`
							<div class="col-md-6 my-3">
								<div class="card">
									<div class="card-body">
										<h5 class="card-title">${course.name}</h5>
										<p class="card-text text-left">
											${course.description}
										</p>
										<p class="card-text text-right">
											${course.price}
										</p>
									</div>
									<div class="card-footer">
										${cardFooter}
									</div>
								</div>
							</div>
						`

					)

			}).join("")

			let container = document.querySelector("#coursesContainer");
			container.innerHTML = courseData;	
		}
	})
}


