let formEdit = document.querySelector("#editCourse");
let params = new URLSearchParams(window.location.search);
let courseId = params.get("courseId");
let token = localStorage.getItem("token");

formEdit.addEventListener("submit", (e) => {
	e.preventDefault()

	let editName = document.querySelector("#courseName").value;
	let editDescription = document.querySelector("#courseDescription").value;
	let editPrice = document.querySelector("#coursePrice").value;
	
	fetch(`https://frozen-badlands-97230.herokuapp.com/api/courses`, {
		method : "PUT",

		headers : {
			"Content-Type" : "application/json",
			"Authorization" : `Bearer ${token}`
		},

		body : JSON.stringify({
			name : editName,
			description : editDescription,
			price :editPrice,
			courseId : courseId
		})
	})
	.then(res => res.json())
	.then(data => {
		if(data) {
			alert("Course Edited")
			window.location.replace("./courses.html")
		} else {
			alert("Something went wrong")
		}
	})
})

