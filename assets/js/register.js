let registerForm = document.querySelector("#registerUser");

registerForm.addEventListener("submit", (e) => {
	e.preventDefault();

	/* get values from the form*/
	let firstName = document.querySelector("#firstName").value
	let lastName = document.querySelector("#lastName").value
	let mobileNo = document.querySelector("#mobileNumber").value
	let email = document.querySelector("#userEmail").value
	let password1 = document.querySelector("#password1").value
	let password2 = document.querySelector("#password2").value

	/* Validate user inputs */
	if((password1 !== "" && password2 !== "") && (password1 === password2) && (mobileNo.length === 11)) {
			/*check if database already has the email*/
			/* fetch() is a js built-in js function that allows getting of data from another source without the need to refresh*/
			/* fetch sends the a request to the url provided with the following parameters
				method -> HTTP Method
				header -> what kind of data to send
				body -> the content of req.body
			*/

			fetch("https://frozen-badlands-97230.herokuapp.com/api/users/email-exists", {

				method : 'POST',

				headers : {

				  'Content-Type': 'application/json'
				 
				},

				body : JSON.stringify({
					email : email
				})

			})

			.then(response => response.json()).then(data => {
				/*if data is false then there are no duplicates*/
				/*if data is true then there duplicates*/
				if(data === false ){
					
					fetch("https://frozen-badlands-97230.herokuapp.com/api/users", {
						method : "POST",

						headers : {
							"Content-Type" : "application/json"
						},

						body : JSON.stringify({
							firstname : firstName,
							lastname : lastName,
							email : email,
							mobileNo : mobileNo,
							password : password1,
						})
					})
					.then(response => response.json())
					.then(data => {
					  	if(data) {
					  		alert("Registration succesful")
					  	} else {
					  		alert("Registration went wrong")
					  	}
					})

				} else {
					alert("Email already exist")
				}
			})

	} else {
		alert("Invalid Inputs");
	}
});

		