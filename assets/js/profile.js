let isAdmin = localStorage.getItem("isAdmin")
let token = localStorage.getItem("token")



fetch("https://frozen-badlands-97230.herokuapp.com/api/users/details", {
	method : "GET",

	headers : {
		"Authorization" : `Bearer ${token}`
	}
})
.then(res => res.json())
.then(data => {

	let firstName = document.querySelector("#firstName");
	let lastName = document.querySelector("#lastName");
	let email = document.querySelector("#email");
	let mobileNumber = document.querySelector("#mobileNumber");
	let enrollContainer = document.querySelector("#enrollContainer")
	

	firstName.innerHTML = ` First Name : ${data.firstname}`
	lastName.innerHTML = `Last Name : ${data.lastname}`
	email.innerHTML = `Email : ${data.email}`
	mobileNumber.innerHTML =`Mobile Number : ${data.mobileNo}`

	let courseData;
	data.enrollments.map(course => {
		
		fetch(`https://frozen-badlands-97230.herokuapp.com/api/courses/${course.courseId}`)
		.then(res => res.json())
		.then(data => {
			courseData = `<p class="text-center">${data.name}</p>`
			return enrollContainer.innerHTML = enrollContainer.innerHTML + courseData
		})
		let enrollContainer = document.querySelector("#enrollContainer")
	})

})	
		
