let params = new URLSearchParams(window.location.search)
let courseId = params.get("courseId")
let isAdmin = localStorage.getItem("isAdmin")
let token = localStorage.getItem("token")

let courseName = document.querySelector("#courseName");
let courseDesc = document.querySelector("#courseDesc");
let coursePrice = document.querySelector("#coursePrice");
let enrollContainer = document.querySelector("#enrollContainer");


if(isAdmin == "false" || !isAdmin) {
	fetch(`https://frozen-badlands-97230.herokuapp.com/api/courses/${courseId}`)
	.then(res => res.json())
	.then(data => {
		courseName.innerHTML = data.name,
		courseDesc.innerHTML = data.description,
		coursePrice.innerHTML = data.price
		enrollContainer.innerHTML = `<button id="enrollButton" class="btn btn-block btn-primary">Enroll</button>`

		document.querySelector("#enrollButton").addEventListener("click", () => {

			fetch("https://frozen-badlands-97230.herokuapp.com/api/users/enroll", {
				method : "POST",

				headers : {
					"Content-Type" : "application/json",
					"Authorization" : `Bearer ${token}`
				},

				body : JSON.stringify({
					courseId : courseId
				})

			})

			.then(res => res.json())
			.then(data => {
					if (data) {
					alert("Your enrolled succesfully")
					window.location.replace("./courses.html")
				} else {
					alert("Enrollment Failed")
				}
			})
		})
	})
} else {
	let container = document.querySelector("#removeLink");
	removeLink.innerHTML = null;

	fetch(`https://frozen-badlands-97230.herokuapp.com/api/courses/${courseId}`)
	.then(res => res.json())
	.then(data => {
		courseName.innerHTML = data.name,
		courseDesc.innerHTML = data.description,
		coursePrice.innerHTML = data.price
		let courseData;

		data.enrollees.map(course => {
			
			fetch(`https://frozen-badlands-97230.herokuapp.com/api/users/${course.userId}`)
			.then(res => res.json())
			.then(data => {
				courseData = `<p class="text-center">${data.firstname} ${data.lastname}</p>`
				return enrollContainer.innerHTML = enrollContainer.innerHTML + courseData

			})
			let enrollContainer = document.querySelector("#enrollContainer")

		})
	})
}

