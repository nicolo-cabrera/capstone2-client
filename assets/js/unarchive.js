let params = new URLSearchParams(window.location.search);
let courseId = params.get("courseId");
let token = localStorage.getItem("token");


fetch(`https://frozen-badlands-97230.herokuapp.com/api/courses/${courseId}`, {
	method : "PUT",

	headers : {
		
		"Authorization" : `Bearer ${token}`
	}

})

.then(res => res.json())
.then(data => {
	if(data) {
		alert("Succes")
		window.location.replace("./courses.html")
	}else {
		alert("Something went wrong")
	}
})